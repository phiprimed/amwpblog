from sys import maxsize as Inf  
# Note : sys.maxint is removed in Python 3.
# sys.maxsize works in both

import json

# Used for BFS
class Queue:
  def __init__(self):
    self.items = []

  def isEmpty(self):
    return len(self.items) == 0

  def inject(self, val):
    self.items.insert(0,val)

  def eject(self):
    return self.items.pop()

  def size(self):
    return len(self.items)

  def display(self):
    print(self.items)

# A dict-based adjacency list representation where the keys are the vertices
# and the value for each key is a list of vertices that are neighbors of that 
# vertex i.e. vertices that have incident edges from the "key" vertex
# If the edges are undirected, then for each edge (u,v), v is present in u's list 
# and u is present in v's list.
# The dict should be traversed in sorted order of key to ensure a fixed order.
def initGraph():
  G = {}
  return G

# Loads a connected graph given the pairs of vertices defining the undirected edges
# in a file whose name is provided in edgesFname
# Since the graph is connected, the set of vertices can be derived from the edge 
# endpoints
def loadGraph(G, edgesFname):
  
  with open(edgesFname) as f:
    for line in f:
      u,v = line.strip().split()
      if G.get(u) == None:
        G[u] = [v]
      else:
        G[u].append(v)
      if G.get(v) == None:
        G[v] = [u]
      else:
        G[v].append(u)

# Loads a disconnected graph given the set of vertices in a file whose name is provided in 
# verticesFname 
# and pairs of vertices defining the undirected edges in a file whose name is provided in
# edgesFname
# Since the graph is disconnected, the set of vertices cannot be derived from the edge 
# endpoints alone       
def loadGraphDisconnected(G, verticesFname, edgesFname):
  with open(verticesFname) as f:
    for line in f:
      v = line.strip()
      G[v] = []
    
    loadGraph(G, edgesFname)

"""
def loadGraphJSONAdjList(G, fname):
  V,E = G
  with open(fname) as f:
    data = json.load(f)

  for country, neighbors in data.iteritems():
    V[country] = neighbors
"""
  
"""
def loadGraphCSVAdjList(G, fname1, fname2):
  V,E = G
  conv = {}
  fout = open('myout.csv','w')
  with open(fname2) as f:
    for line in f:
      toks = line.strip().split(',')
      conv[toks[3]] = toks[2]
  with open(fname1) as f:
    for line in f:
      toks = line.strip().split(' ')
      newToks = []
      for i in range(len(toks)):
        if conv.get(toks[i]) == None:
          continue
        else:
          newToks.append(conv[toks[i]])

      if len(newToks) > 0:
        V[newToks[0]] = newToks[1:]
        print >> fout, ' '.join(newToks)
  fout.close()
"""

def loadGraphFromAdjList(G, fname):
  with open(fname) as f:
    for line in f:
      toks = line.strip().split(' ')
      G[toks[0]] = toks[1:]

def saveGraphAsAdjList(G, fname):
   with open(fname, 'w') as f:
     for u in sorted(G.keys()):
       neighbors = G[u] 
       f.write(u)
       if len(neighbors) > 0 :
         f.write(' ' + ' '.join(neighbors))
       f.write('\n')      
      
def bfs(G, s):
  Q = Queue()

  dist = {x:Inf for x in G.keys()}
  prev = {x: None for x in G.keys()}
  dist[s] = 0
  prev[s] = None
  Q.inject(s)

  while not Q.isEmpty():
    v = Q.eject()
    
    nexts = filter(lambda x: dist[x] == Inf , G[v])
    for next in nexts:
      dist[next] = dist[v] + 1
      prev[next] = v
      Q.inject(next)

  assert len(list(filter(lambda x: x == Inf, dist))) == 0

  return [(k, dist[k], prev[k]) for k in sorted(dist, key=dist.get)]

  
def dfs_explore(G, u, visited, pre=None, post=None, cc=None, clock=None, ccnum=None) :

   visited[u] = True

   dfs_previsit(u, pre, cc, clock, ccnum)

   for v in G[u]:
     if not visited[v]:
       dfs_explore(G, v, visited, pre, post, cc, clock, ccnum)
     #else:
       #print('Back edge ' + v + ',' + u)

   dfs_postvisit(u, post, clock)

def dfs_previsit(u, pre, cc, clock, ccnum):
   if pre != None:
     pre[u] = clock[0]
     clock[0] += 1
   if cc != None:
     cc[u] = ccnum[0]
     

def dfs_postvisit(u, post, clock):
   if post != None:
     post[u] = clock[0]
     clock[0] += 1
  

def dfs_main_connected(G, s):
   visited = {x: False for x in G.keys()}
   clock = [0]  # a 1-element list to work around call by value for int
   pre = {x:-1 for x in G.keys()}
   post = {x:-1 for x in G.keys()}
  
   dfs_explore(G, s, visited, pre=pre, post=post, clock=clock)

   assert list(filter(lambda x: not x, visited.values())) == []

   assert list(filter(lambda x: x < 0 or x >= 2*len(G), pre.values())) == []
   assert list(filter(lambda x: x < 0 or x >= 2*len(G), post.values())) == []

   return post, pre

def dfs_main_disconnected(G):
   visited = {x: False for x in G.keys()}
   cc = {x:-1 for x in G.keys()}
   ccnum = [0]

   for v in sorted(G.keys()):
     if not visited[v]:
       print("Calling dfs_explore from " + v + " ccnum=" + str(ccnum[0]))
       dfs_explore(G, v, visited, cc=cc, ccnum=ccnum)
       ccnum[0]+=1

   return cc


def main():

  """
  US contiguity dataset processing
  
  """
  G1 = initGraph()
  
  loadGraph(G1, 'data/contiguous-usa.dat')
  
  # Saved for understanding and debugging purposes
  saveGraphAsAdjList(G1, 'output/adjacency-computed-usa.dat')

  # Start BFS from each of the below states
  for s in ['NY', 'CA', 'NE', 'ME']:
    with open('output/bfs_' + s + '.txt', 'w') as f:
      f.write('state,hops,pred\n')
      for state, dist, pred in bfs(G1, s):
        f.write(','.join([str(state), str(dist), str(pred)]) + '\n')

  #post, pre = dfs_main_connected(G1, "NY")

  #print("DFS from NY (pre): " + str(sorted(pre, key=pre.get)))
  #print('\n')
  #print("DFS from NY (post): " + str(sorted(post, key=post.get)))
  #print('\n')
  
  """
  World Correlates of War dataset processing
  
  """
  G2 = initGraph()

  loadGraphDisconnected(G2, 'data/all-countries-iso3.dat', 'data/country-borders-landorriver-iso3.dat')
  
  # Saved for understanding and debugging purposes
  saveGraphAsAdjList(G2, 'output/adjacency-computed-world.dat')

  # identify the connected components and write results 
  cc = dfs_main_disconnected(G2)
  with open('output/cc_world.txt', 'w') as f:
    f.write('country,ccid\n')
    for country in sorted(cc, key=cc.get):
      f.write(country + ',' + str(cc[country]) + '\n')
      
  G3 = initGraph()
  loadGraph(G3, 'data/contiguous-india-states-no-TS.dat')
  # Start BFS from each of the below states
  for s in ['JK', 'GJ', 'MP', 'AR']:
    with open('output/bfs_india_' + s + '.txt', 'w') as f:
      f.write('state,hops,pred\n')
      for state, dist, pred in bfs(G3, s):
        f.write(','.join([str(state), str(dist), str(pred)]) + '\n')
  
if __name__  == "__main__":
  main()
