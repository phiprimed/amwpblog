# Maps and simple graphs

https://amukherjeeworld.wordpress.com/2017/05/25/maps-and-simple-graphs-part-1/

## Source files
- worldCountriesProcessor.R
- graphProcessor.py
- usa_map.html
- world_map.html

## Data
- contiguous-usa.dat
- contdir.csv
    - all-countries-iso3.dat
    - country-borders-landorriver-iso3.dat
- Direct Contiguity Codebook.pdf
  

## Output
- bfs_[NY|NE|CA|ME].txt
- cc_world.txt
- adjacency-computed-usa.dat
- adjacency-computed-world.dat